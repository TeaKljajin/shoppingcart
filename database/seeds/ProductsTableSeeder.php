<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'MacBook Pro',
            'slug' => 'macbook-pro',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 24999,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. Lorem Ipsum has been the industry\'s standard dummy
             text ever since the 1500s, when an unknown printer took a galley of type and
             scrambled it to make a type specimen book.',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 1',
            'slug' => 'laptop-1',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 4999,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. Lorem Ipsum has been the industry\'s standard dummy
             text ever since the 1500s, when an unknown printer took a galley of type and
             scrambled it to make a type specimen book.',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 2',
            'slug' => 'laptop-2',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 14999,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. Lorem Ipsum has been the industry\'s standard dummy
             text ever since the 1500s, when an unknown printer took a galley of type and
             scrambled it to make a type specimen book.',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 3',
            'slug' => 'laptop-3',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 34999,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. ',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 4',
            'slug' => 'laptop-4',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 44999,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. ',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 5',
            'slug' => 'laptop-5',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 44779,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. ',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 6',
            'slug' => 'laptop-6',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 1279,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. ',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 8',
            'slug' => 'laptop-8',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 4339,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. ',
            'category_id'=> 1,
        ]);
        Product::create([
            'name' => 'Laptop 9',
            'slug' => 'laptop-9',
            'details' => '15inch, 1TB SSD, 32 GB RAM',
            'price'=> 4779,
            'description' => 'Lorem Ipsum is simply dummy text of the printing and
             typesetting industry. ',
            'category_id'=> 1,
        ]);
    }
}
