<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Public
Route::get('/','PublicController@index')->name('public.index');
Route::get('/about','PublicController@about')->name('public.about');

//Shop
Route::get('/shop','ShopController@index')->name('shop.index');
Route::get('/shop/product/{slug}','ShopController@show')->name('shop.show');

//Cart
Route::get('/cart/','CartController@index')->name('cart.index');
Route::post('/cart/store','CartController@store')->name('cart.store');
Route::post('/cart/store/saveForLater/{product}','CartController@saveForLater')->name('cart.saveForLater');
Route::post('/cart/destroy/{product}','CartController@destroy')->name('cart.destroy');
Route::get('/cart/empty/','CartController@empty')->name('cart.empty');
Route::get('/checkout','CartController@checkout')->name('cart.checkout');
Route::get('/thank','CartController@thank')->name('cart.thank');

//Save For Later
Route::post('/cart/saveForLater/store/moveToCart/{product}','SaveForLaterController@moveToCart')->name('saveForLater.moveToCart');
Route::post('/cart/saveForLater/destroy/{product}','SaveForLaterController@destroy')->name('saveForLater.destroy');
Route::get('/cart/saveForLater/empty/','SaveForLaterController@empty')->name('saveForLater.empty');

