@extends('front.layouts.master')
@section('title') E-commerce @endsection
@section('content')



    <div class="container">
        <h1 class="text-center mt-5">Laravel E-coomerce</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
            took a galley of type and scrambled it to make a type specimen book. It has survived not only five
            centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was
            popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
            more recently with desktop publishing software like Aldus PageMaker including versions of Lorem
            Ipsum.
        </p>

        <div class="text-center mt-5">
            <a type="button" class="btn btn-outline-secondary" href="#" id="onF">Features</a>
            <a type="button" class="btn btn-outline-secondary" href="#" id="onS">On Sale</a>
        </div>

        <div class="row mt-5">
            @foreach($products as $product)
            <div class="col-md-3 text-center">
                <a href="{{route('shop.show',$product->slug)}}"><img src="{{asset('img/2.png')}}" alt="product" class="img"></a><br>
                <a href="{{route('shop.show',$product->slug)}}"><span class="product-name">{{$product->name}}</span></a>
                <div>${{$product->price}}</div>
            </div>
            @endforeach

        </div>


        <div class="text-center mt-5 mb-5">
            <a type="button" class="btn btn-outline-secondary" href="{{route('shop.index')}}" id="onV">View More Products</a>
        </div>
    </div><!---end container--->






    <div class="container" id="thr">
        <h1 class="text-center">From Our Blog</h1>
        <div class="row mt-5">
            <div class="col-md-4">
                <a href="#" class="img3"><img src="img/2.png" alt="blog_image" class="img img-responsive"></a>
                <a href="#"><h2 class="text-center">Blog Post Title 1</h2></a>
                <p class="p1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                    took a galley of type and scrambled it to make a type specimen book.
                </p>
            </div>
            <div class="col-md-4">
                <a href="#" class="img3"><img src="img/2.png" alt="blog_image" class="img"></a>
                <a href="#"><h2 class="text-center">Blog Post Title 1</h2></a>
                <p class="p1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                    took a galley of type and scrambled it to make a type specimen book.
                </p>
            </div>
            <div class="col-md-4">
                <a href="#" class="img3"><img src="img/2.png" alt="blog_image" class="img"></a>
                <a href="#"><h2 class="text-center">Blog Post Title 1</h2></a>
                <p class="p1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                    took a galley of type and scrambled it to make a type specimen book.
                </p>
            </div>
        </div>
    </div>



@endsection
