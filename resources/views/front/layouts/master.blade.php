<!doctype html>
<html lang="en">
    @include('front.includes.head')
<body>
<header>
    @include('front.includes.navbar')

    @if(Request::is('/'))
        @include('front.includes.header')
    @endif



</header>
    @yield('content')
    @include('front.includes.footer')
</body>
</html>

