<!---Top nav---->
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container">
        <a class="myNav" href="{{route('public.index')}}">Laravel E-commerce</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
{{--                <li class="nav-item active">--}}
{{--                    <a class="myNav" href="#">Home </a>--}}
{{--                </li>--}}


            </ul>
        </div>
        <div>
            <ul class="navbar-nav pull-right">
                <li><a href="{{route('public.about')}}" class="myNav">About</a></li>
                <li><a href="#" class="myNav">Blog</a></li>
                <li><a href="{{route('shop.index')}}" class="myNav">Shop</a></li>
                <li><a href="{{route('cart.index')}}" class="myNav">Cart
                        @if(Cart::instance('default')->count() > 0)
                        ({{Cart::instance('default')->count()}})</a></li>
                        @endif

            </ul>
        </div>
    </div>
</nav><!----End Top Nav--->
