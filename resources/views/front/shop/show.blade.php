@extends('front.layouts.master')
@section('title') {{$product->name}} @endsection
@section('content')

    <div class="container-fluid" id="shop">
        <div class="container">
            <h6><a href="{{route('public.index')}}" class="mr12">Home</a> > <a href="{{route('shop.index')}}" class="mr12">Shop</a> > MacBook Pro</h6>
        </div>
    </div>

    <div class="col-lg-12">
        @include('front.includes.messages')
    </div>

    <div class="container" id="mr">
        <div class="row">
            <div class="col-lg-5" id="mr2">
                <img src="{{asset('img/products/1.png')}}" id="mr1" alt="Slika">
            </div>
            <div class="col-lg-7" id="mr4">
                <h3>{{$product->name}}</h3>
                <h6 id="mr13">{{$product->details}}</h6>
                <h1 id="mr14">${{$product->price}}</h1>
                <p>{{$product->description}}
                </p>

                <form method="post" action="{{route('cart.store')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$product->id}}">
                    <input type="hidden" name="name" value="{{$product->name}}">
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <button type="submit" class="button  btn-outline-secondary" id="mr5">Add To Cart</button>
                </form>

            </div>
        </div>
    </div>


    <!---You my Also Like--->
    <div class="container-fluid" id="mr6">
        <div class="container">
         <h5 class="mr11">You might also like...</h5>
            <div class="row mt-1" id="mr9">
                @foreach($mightAlsoLike as $product)
                <div class="col-md-3 mr8"><a href="{{route('shop.show',$product->slug)}}" class="mr12">
                    <div class="mr10">
                        <img src="{{asset('img/products/1.png')}}" class="mr7" alt="Slika">
                        <p class="text-center mb-1" ><b>{{$product->name}}</b></p>
                        <p class="text-center mb-1">${{$product->price}}</p>
                    </div></a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!---End You my Also Like--->
@endsection
