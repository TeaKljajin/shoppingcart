@extends('front.layouts.master')
@section('title') Shop @endsection
@section('content')

    <div class="container-fluid" id="shop">
        <div class="container">
            <h6><a href="{{route('public.index')}}" class="mr12">Home</a> > Shop</h6>
        </div>
    </div>
<div class="container-fluid" id="shop2">
    <div class="container" id="shop3">
        <div class="row">
            <div class="col-lg-3">
                <h5 class="pr2">By Category</h5>
                <ul class="pr1">
                    <li>Laptops</li>
                    <li>Desktops</li>
                    <li>Mobile Phones</li>
                    <li>Tablets</li>
                    <li>TVs</li>
                    <li>Digital Cameras</li>
                    <li>Appliances</li>
                </ul>
                <h5 class="pr2">By Price</h5>
                <ul class="pr1">
                    <li>$0-700</li>
                    <li>$700-2500</li>
                    <li>$2500-4000</li>
                    <li>$4000+</li>
                </ul>
            </div>
            <div class="col-lg-9" id="pr5">
                <h3 id="pr3">Laptops</h3>
                <div class="row mt-5">
                    @foreach($products as $product)
                        <div class="col-md-4 text-center mt-4">
                            <a href="{{route('shop.show',$product->slug)}}"><img src="img/2.png" alt="product" class="pr4"></a><br>
                            <a href="{{route('shop.show',$product->slug)}}" class="pr6">{{$product->name}}</a>
                            <div class="pr7">${{$product->price}}</div>
                        </div>
                    @endforeach
                </div>
                <div class="text-center mt-5 mb-5" >
                    <div id="post_data" class="pr8">View More Products</div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

