@extends('front.layouts.master')
@section('title') Thank You @endsection
@section('content')

    <div class="container-fluid" id="li">
        <div class="container" id="li1">
            <div class="li2">
                <h2 class="text-center">Thank you for Your order</h2>
                <p class="text-center" >A confirmation email was sent</p>
                <a type="button" class="btn btn-outline-secondary" id="li3" href="{{route('public.index')}}" >Home Page</a>
            </div>
        </div>
    </div>
@endsection
