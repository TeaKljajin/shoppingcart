@extends('front.layouts.master')
@section('title') Cart @endsection
@section('content')

    <div class="container-fluid" id="shop">
        <div class="container">
            <h6><a href="{{route('public.index')}}" class="mr12">Home</a> > Shopping Cart</h6>
        </div>
    </div>



    <!----Shopping Cart---->
    <div class="container-fluid" id="ni14">
    <div class="container ni15">

      <div class="col-lg-8">
        @include('front.includes.messages')
      </div>

        <div class="row mt-5">
            <div class="col-lg-8">


                @if(Cart::count() > 0)
                    <h4>{{Cart::count()}} item(s) in Shopping Cart</h4>
                @foreach(Cart::content() as $item)
                    <table class="table">
                        <tbody>
                        <tr>
                            <!------For image-------asset('img/products/'.$item->model->slug.'.jpg'------->
                            <td><a href="{{route('shop.show',$item->model->slug)}}" ><img src="{{asset('img/products/1.png')}}" alt="Slika" id="ni"></a></td>
                            <td><a href="{{route('shop.show',$item->model->slug)}}" class="ni16">{{$item->model->name}}</a><br>
                                <a href="{{route('shop.show',$item->model->slug)}}" class="ni17">{{$item->model->details}}</a>
                            </td>
                            <td >
                                <form method="post" action="{{route('cart.destroy',$item->rowId)}}" class="mt-3">
                                  {{csrf_field()}}
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                </form><br>
                            </td>
                            <td>
                                <form method="post" action="{{route('cart.saveForLater',$item->rowId)}}" class="mt-3">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-outline-primary btn-sm">Save for later</button>
                                </form>
                            </td>

                            <td>
                                <select class="mt-3">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>
                            <td class="ni20"><div class="mt-3">${{$item->model->price}}</div></td>
                        </tr>
                        </tbody>
                    </table>
                    @endforeach
                   @else
                    <h3>No item(s) in Cart!</h3>

                   @endif

            </div>
        </div>
    </div>
    <!----End Shopping Cart---->

    <!---Cupon--->
    <div class="container ni15" >
        <div class="row" id="ni12">
            <div class="col-lg-8" id="ni1">
                <h4 id="ni3" class="pull-left">Have a code?</h4>
                <button class="btn btn-primary  pull-right" type="button">Submit</button>
                <input type="text" class="form-control col-lg-4 pull-right" placeholder="" aria-label="" aria-describedby="basic-addon1">

            </div>
        </div>
    </div>
    <!---End Cupon--->


    <!---Price--->
    <div class="container ni15" >
        <div class="row" >
            <div class="col-lg-8">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th>SubTotal</th>
                        <th>Tax (2%)</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>

                        <td>${{Cart::subtotal()}}</td>
                        <td>${{Cart::tax()}}</td>
                        <td>${{Cart::total()}}</td>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!---End Price--->



    <!---Buttons ---->
    <div class="container ni15">
        <div class="row">
            <div class="col-lg-8">
                <a type="button" class="btn btn-secondary" href="{{route('shop.index')}}" id="niL">Continue Shopping</a>
                <a type="button" class="btn btn-success btn-info pull-right" href="{{route('cart.checkout')}}" id="niR">Proceed to Checkout</a>
            </div>
        </div>
    </div>
    <!---End Buttons ---->




    <!---Save For Later----->
    <div class="container ni15">
        <div class="row">
            <div class="col-lg-8">
                @if(Cart::instance('saveForLater')->count() > 0)
                    <h4>{{Cart::instance('saveForLater')->count()}} item(s) in Shopping Cart</h4>
                @foreach(Cart::instance('saveForLater')->content() as $item)
                        <table class="table">
                            <tbody>
                            <tr>
                                <!------For image-------asset('img/products/'.$item->model->slug.'.jpg'------->
                                <td><a href="{{route('shop.show',$item->model->slug)}}" ><img src="{{asset('img/products/1.png')}}" alt="Slika" id="ni"></a></td>
                                <td><a href="{{route('shop.show',$item->model->slug)}}" class="ni16">{{$item->model->name}}</a><br>
                                    <a href="{{route('shop.show',$item->model->slug)}}" class="ni17">{{$item->model->details}}</a>
                                </td>
                                <td >
                                    <form method="post" action="{{route('saveForLater.destroy',$item->rowId)}}" class="mt-3">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                    </form><br>
                                </td>
                                <td>
                                    <form method="post" action="{{route('saveForLater.moveToCart',$item->rowId)}}" class="mt-3">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-outline-primary btn-sm">Move to Cart</button>
                                    </form>
                                </td>

                                <td>
                                    <select class="mt-3">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                    </select>
                                </td>
                                <td class="ni20"><div class="mt-3">${{$item->model->price}}</div></td>
                            </tr>
                            </tbody>
                        </table>
                @endforeach
                    @else
                    <h3>No item(s) in Save For Later!</h3>
                @endif
            </div>
        </div>
    </div> <!---End Save For Later----->




        <!---You my Also Like--->
        <div class="container-fluid" id="mr6">
            <div class="container">
                <h5 class="mr11">You might also like...</h5>
                <div class="row mt-1" id="mr9">
                    @foreach($mightAlsoLike as $product)
                        <div class="col-md-3 mr8"><a href="{{route('shop.show',$product->slug)}}" class="mr12">
                                <div class="mr10">
                                    <img src="{{asset('img/products/1.png')}}" class="mr7" alt="Slika">
                                    <p class="text-center mb-1"><b>{{$product->name}}</b></p>
                                    <p class="text-center mb-1">${{$product->price}}</p>
                                </div></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <!---End You my Also Like--->
    </div>

@endsection
