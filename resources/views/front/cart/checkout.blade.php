@extends('front.layouts.master')
@section('title') Checkout @endsection
@section('content')

    <div class="container-fluid" id="shop">
        <div class="container">
            <h6><a href="{{route('public.index')}}" class="mr12">Home</a> > Checkout</h6>
        </div>
    </div>


    <div class="container-fluid" id="bi">
        <div class="container" id="bi1">
           <h3 class="bi2">Checkout</h3>
            <div class="row mt-5">
                <div class="col-lg-6">
                    <h5>Billing Details</h5>
                    <form method="post" action="#">
                        <div class="form-group">
                            <lagel for="name">Name</lagel><br>
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <lagel for="email">Email</lagel><br>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <lagel for="address">Address</lagel><br>
                            <input type="text" name="address" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <lagel for="city">City</lagel><br>
                                    <input type="text" name="city" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <lagel for="province">Province</lagel><br>
                                    <input type="text" name="province" class="form-control">
                                </div>
                            </div>
                        </div>
                       <div class="row">
                           <div class="col-lg-6">
                               <div class="form-group">
                                   <lagel for="postal">Postal code</lagel><br>
                                   <input type="numeric" name="postal" class="form-control">
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="form-group">
                                   <lagel for="phone">Phone</lagel><br>
                                   <input type="text" name="phone" class="form-control">
                               </div>
                           </div>
                       </div>
                 <!----Payment details------->
                    <h4>Payment details</h4>
                    <div class="form-group">
                        <lagel for="name_on_card">Name on Card</lagel><br>
                        <input type="text" name="name_on_card" class="form-control">
                    </div>
                    <div class="form-group">
                        <lagel for="address">Address</lagel><br>
                        <input type="text" name="address" class="form-control">
                    </div>
                    </form>
                    <!----End Payment details------->

                </div>



                <div class="col-lg-6">
                    <h5>Your Order</h5>

                    <!--------Your Order------->
                    <table class="table">
                        <tbody>
                        <tr>
                            <td><img src="{{asset('img/1.png')}}" alt="Slika" id="ni"></td>
                            <td>
                                <ul class="bi3">
                                    <li class="bi4">MacBook Pro</li>
                                    <li>15inch, 1TB SSD, 32 GB RAM</li>
                                    <li>$786</li>
                                </ul>
                            </td>
                            <td >
                                <select class="mt-3">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td><img src="{{asset('img/1.png')}}" alt="Slika" id="ni"></td>
                            <td>
                                <ul class="bi3">
                                    <li class="bi4">MacBook Pro</li>
                                    <li>15inch, 1TB SSD, 32 GB RAM</li>
                                    <li>$786</li>
                                </ul>
                            </td>
                            <td>
                                <select class="mt-3">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="{{asset('img/1.png')}}" alt="Slika" id="ni"></td>
                            <td>
                                <ul class="bi3">
                                    <li class="bi4">MacBook Pro</li>
                                    <li>15inch, 1TB SSD, 32 GB RAM</li>
                                    <li>$786</li>
                                </ul>
                            </td>
                            <td>
                                <select class="mt-3">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <!--------End Your Order------->




                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            <h6>SubTotal</h6>
                            <h6>Discount</h6>
                            <h6>Tax</h6>
                            <h6 class="bi6">Total</h6>
                        </div>
                        <div class="col-lg-6">
                            <p class="bi5">$76768</p>
                            <p class="bi5">$78</p>
                            <p class="bi5">64</p>
                            <p class="bi5 bi6">$7676</p>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>




@endsection
