-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2020 at 11:57 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelstripe`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2014_10_12_000000_create_users_table', 1),
(9, '2020_07_30_113214_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `details`, `price`, `description`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'MacBook Pro', 'macbook-pro', '15inch, 1TB SSD, 32 GB RAM', 24999, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. Lorem Ipsum has been the industry\'s standard dummy\n             text ever since the 1500s, when an unknown printer took a galley of type and\n             scrambled it to make a type specimen book.', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(2, 'Laptop 1', 'laptop-1', '15inch, 1TB SSD, 32 GB RAM', 4999, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. Lorem Ipsum has been the industry\'s standard dummy\n             text ever since the 1500s, when an unknown printer took a galley of type and\n             scrambled it to make a type specimen book.', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(3, 'Laptop 2', 'laptop-2', '15inch, 1TB SSD, 32 GB RAM', 14999, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. Lorem Ipsum has been the industry\'s standard dummy\n             text ever since the 1500s, when an unknown printer took a galley of type and\n             scrambled it to make a type specimen book.', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(4, 'Laptop 3', 'laptop-3', '15inch, 1TB SSD, 32 GB RAM', 34999, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. ', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(5, 'Laptop 4', 'laptop-4', '15inch, 1TB SSD, 32 GB RAM', 44999, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. ', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(6, 'Laptop 5', 'laptop-5', '15inch, 1TB SSD, 32 GB RAM', 44779, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. ', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(7, 'Laptop 6', 'laptop-6', '15inch, 1TB SSD, 32 GB RAM', 1279, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. ', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(8, 'Laptop 8', 'laptop-8', '15inch, 1TB SSD, 32 GB RAM', 4339, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. ', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40'),
(9, 'Laptop 9', 'laptop-9', '15inch, 1TB SSD, 32 GB RAM', 4779, 'Lorem Ipsum is simply dummy text of the printing and\n             typesetting industry. ', 1, '2020-07-30 12:29:40', '2020-07-30 12:29:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
