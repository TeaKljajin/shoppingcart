<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class SaveForLaterController extends Controller
{
    public function moveToCart($id){
        //Find the product
        $item = Cart::instance('saveForLater')->get($id);
        //Remove the product
        Cart::instance('saveForLater')->remove($id);

        //Can not add same product to cart
        $duplicates = Cart::instance('default')->search(function($cartItem, $rowId) use ($id){
            return $rowId  === $id;
        });
        if($duplicates->isNotEmpty()){
            session()->flash('msg','Item is already in your Cart');
            return redirect()->route('cart.index');
        }
        //Add product to Save For Later
        Cart::instance('default')->add($item->id, $item->name, 1, $item->price)->associate('App\Product');
        session()->flash('msg','Item has been moved to your Cart');
        return redirect()->route('cart.index');

    }

    public function destroy($id)
    {

        Cart::instance('saveForLater')->remove($id);
        session()->flash('msg','Item has been removed from Save for Later');
        return redirect()->back();
    }

    public function empty(){
        Card::instance('saveForLater')->destroy();
    }
}
