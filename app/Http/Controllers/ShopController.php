<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function index(){
        $products = Product::inRandomOrder()->take(9)->get();
        return view('front.shop.index',compact('products'));

    }

    public function show($slug){
        //That url be the same as product slug name
        $product = Product::where('slug',$slug)->first();
        //You might Also like section
        $mightAlsoLike = Product::where('slug','!=',$slug)->inRandomOrder()->take(4)->get();

        return view('front.shop.show',compact('product','mightAlsoLike'));
    }





}
