<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class PublicController extends Controller
{
    public function index(){
        $products = Product::inRandomOrder()->take(8)->get();
        return view('index',compact('products'));
    }
    public function about(){
        return view('about');
    }




}
