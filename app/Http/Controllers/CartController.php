<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //You might Also like section
        $mightAlsoLike = Product::inRandomOrder()->take(4)->get();
        return view('front.cart.index',compact('mightAlsoLike'));


    }

    public function checkout(){
        return view('front.cart.checkout');
    }

    public function thank(){
        return view('front.cart.thank');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //Can not add same product to cart
        $duplicates = Cart::search(function($cartItem, $rowId) use ($request){
               return $cartItem->id  === $request->id;
        });
        if($duplicates->isNotEmpty()){
            session()->flash('msg','Item is already in your cart');
            return redirect()->route('cart.index');
        }
        //Add product to Cart
        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');
        session()->flash('msg','Item was added to your cart');
        return redirect()->route('cart.index');


    }

    public function saveForLater($id){
        //Find the product
        $item = Cart::get($id);
        //Remove the product
        Cart::remove($id);

        //Can not add same product to cart
        $duplicates = Cart::instance('saveForLater')->search(function($cartItem, $rowId) use ($id){
            return $rowId  === $id;
        });
        if($duplicates->isNotEmpty()){
            session()->flash('msg','Item is already Saved for later');
            return redirect()->route('cart.index');
        }
        //Add product to Save For Later
        Cart::instance('saveForLater')->add($item->id, $item->name, 1, $item->price)->associate('App\Product');
        session()->flash('msg','Item has been Save For Later');
        return redirect()->route('cart.index');


    }

    public function empty(){
        Card::destroy();
    }



    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Cart::remove($id);
        session()->flash('msg','Item has been removed from your card');
        return redirect()->back();
    }
}
